#include "metarReader.h"

static const char *host = "weather.noaa.gov";
static const char *metarDir = "/data/observations/metar/stations/";

cUpdate::cUpdate(cWeatherSetup *setup) {
	this->setup = setup;
	active = false;
}

cUpdate::~cUpdate() {
    if (active) {
		active = false;
		Cancel(3);
    }
}

inline const char *cUpdate::getMetar() {
	if (fetched) {
		return metar;
	} else {
		throw NoDataAvailable();
	}
}

Decoded_METAR *cUpdate::getDecodedMetar() {
	if (fetched) {
		return &decodedMetar;
	} else {
		throw NoDataAvailable();
	}
}

inline const char *cUpdate::getDate() {
	if (fetched) {
		return date;
	} else {
		throw NoDataAvailable();
	}
}

bool cUpdate::isDataAvailable() {
	return fetched;
}

void cUpdate::StartUpdate() {
    if (!active) {
		Start();
	} else {
		active = false;
		Cancel(3);
		usleep(250);
		Start();
	}
}

void cUpdate::Action(void) {
	dsyslog("weather Thread started (pid=%d)", getpid());
   active = true;
	do {
		updateWeatherData();
		if (setup->updateTime > 0) {
			for (long i = 0; active && i < setup->updateTime * 60 * 2; i++) {
				usleep(500000);
	    	}
		}
	} while (active && setup->updateTime > 0);

	active = false;
	dsyslog("weather Thread ended (pid=%d)", getpid());
	Cancel(0);
}

void cUpdate::updateWeatherData() {
	char filename[150];
	netbuf *conn = NULL;
	netbuf *nData = NULL;

	FtpInit();
	dsyslog("Connectiong %s.", host);

	if (FtpConnect(host, &conn) == 0) {
		dsyslog("Could not connect server: %s", host);
		logLastResponse(conn);

		return;
	}

	logLastResponse(conn);
	dsyslog("Login user [%s].", setup->user);

	if (FtpLogin(setup->user, setup->pwd, conn) == 0) {
		dsyslog("Could not login user: %s", setup->user);
		logLastResponse(conn);
		FtpQuit(conn);

		return;
	}

	logLastResponse(conn);
	dsyslog("Setting connection mode: %s", (setup->passiveFTP ? "PASV" : "PORT"));

	if (FtpOptions(FTPLIB_CONNMODE, (setup->passiveFTP ? FTPLIB_PASSIVE : FTPLIB_PORT), conn) == 0) {
		dsyslog("Could not set connection mode %s.", (setup->passiveFTP ? "PASV" : "PORT"));
		logLastResponse(conn);
		FtpQuit(conn);

		return;
	}

	sprintf(filename, "%s%s.TXT", metarDir, setup->stationId);
	dsyslog("Getting %s.", filename);

	if (FtpAccess(filename, FTPLIB_FILE_READ, FTPLIB_ASCII, conn, &nData) == 0) {
		dsyslog("Could not acces file: %s", filename);
		logLastResponse(conn);
		FtpQuit(conn);

		return;
	}

	logLastResponse(conn);
	dsyslog("Reading date.");

	if (FtpRead(date, 1024, nData) == 0) {
		dsyslog("Could not read metar");
		logLastResponse(conn);
		FtpQuit(conn);

		return;
	}

	dsyslog("Reading metar.");

	if (FtpRead(metar, 1024, nData) == 0) {
		dsyslog("Could not read metar");
		logLastResponse(conn);
		FtpQuit(conn);

		return;
	}

	logLastResponse(conn);
	dsyslog("Closing connection.");

	if (FtpClose(nData) == 0) {
		dsyslog("Could not close file: %s", filename);
		logLastResponse(conn);
		FtpQuit(conn);

		return;
	}

	logLastResponse(conn);
	dsyslog("Fetched METAR: %s", metar);
	DcdMETAR(metar, &decodedMetar);

	char tmp[3000];

	sprint_metar(tmp, &decodedMetar);
	dsyslog("Decoded METAR: %s", tmp);

	fetched = true;
	FtpQuit(conn);
}

void cUpdate::logLastResponse(netbuf *conn) {
	dsyslog("Response was: \n%s", FtpLastResponse(conn));
}

