#ifndef PLUGIN_WEATHER_H
#define PLUGIN_WEATHER_H

#include <vdr/plugin.h>
#include "setup.h"
#include "metarReader.h"
#define MAXINT       INT_MAX           /* maximum integer */



class cPluginWeather : public cPlugin {
private:
  cWeatherSetup setup;
  cUpdate metarReader;
public:
  cPluginWeather(void);
  virtual ~cPluginWeather();
  virtual const char *Version(void);
  virtual const char *Description(void) { return tr("Displays the current weather conditons."); }
  virtual const char *CommandLineHelp(void);
  virtual bool ProcessArgs(int argc, char *argv[]);
  virtual bool Start(void);
  virtual void Housekeeping(void);
  virtual const char *MainMenuEntry(void) { return tr("Weather"); }
  virtual cOsdObject *MainMenuAction(void);
  virtual cMenuSetupPage *SetupMenu(void);
  virtual bool SetupParse(const char *Name, const char *Value);
  void handleConfigUpdate();
  };

  class cMenuSetupWeather : public cMenuSetupPage {
	private:
		cWeatherSetup *setup;
		cPluginWeather *plugin;
	protected:
		virtual void Store(void);
	public:
  		cMenuSetupWeather(cWeatherSetup *setup, cPluginWeather *plugin);
};


#endif //PLUGIN_WEATHER_H
