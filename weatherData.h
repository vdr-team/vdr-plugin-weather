struct cWeatherData {
	double temp;
	double dew_pt_temp;
	double windSpeed;
	double windDir;
	double ob_date;
	double hour;
	double ob_minute;
	double inches_altstng;
	double hectoPasc_altstng;
	char *stnid;
	char *windUnits;
};