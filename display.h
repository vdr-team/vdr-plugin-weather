#ifndef WEATHER_DISPLAY_H
#define WEATHER_DISPLAY_H

#include "metarReader.h"
#include "weatherUtil.h"
#include "weather.h"
#include "simpleDisplay.h"
#include <string>

class cWeatherOsd : public cSimpleWeatherOsd {
	public:
		cWeatherOsd(cUpdate *metarReader);
	protected:
		virtual void printMetar (cOsdBase *osd, Decoded_METAR *Mptr, int x, int y);
		void initWeatherBitmap(cBitmap &b, Decoded_METAR *mptr, eDvbColor col, bool setBg);
		void initBitmap(cBitmap &b, char *xpm[], eDvbColor col, bool setBg);
		void initNorthBitmap(cBitmap &b, eDvbColor col);
		void initSouthBitmap(cBitmap &b, eDvbColor col);
		void initWestBitmap(cBitmap &b, eDvbColor col);
		void initEastBitmap(cBitmap &b, eDvbColor col);
		void initNorthWestBitmap(cBitmap &b, eDvbColor col);
		void initNorthEastBitmap(cBitmap &b, eDvbColor col);
		void initSouthWestBitmap(cBitmap &b, eDvbColor col);
		void initSouthEastBitmap(cBitmap &b, eDvbColor col);
		void initBorderBitmap(cBitmap &b, eDvbColor col);
		void drawWindRose(cOsdBase *osd, int degree, int x, int y);
		void drawWeather(cOsdBase *osd, Decoded_METAR *mptr, int x, int y);
		std::string cWeatherOsd::getIntensity(const std::string weather);
		std::string cWeatherOsd::getPrecipitation(const std::string weather);
		std::string cWeatherOsd::getCoverage(const std::string weather);
};

#endif //WEATHER_DISPLAY_H
