#include <math.h>
#include "weatherUtil.h"
#include "i18n.h"

int cWeatherUtil::calculateHumidity(double temperature, double dewPoint) {
	return (int) (100.0 * cWeatherUtil::calculateSaturationVaporPressure(dewPoint) / cWeatherUtil::calculateSaturationVaporPressure(temperature));
}

double cWeatherUtil::calculateSaturationVaporPressure(double temperature) {
	double a, b;

	if (temperature >= 0) {
		a = 7.5;
		b = 237.3;
	} else {
		a = 7.6;
		b = 240.7;
	}

	return 6.1078 * pow(10, (a*temperature)/(b+temperature));
}

const char *cWeatherUtil::calculateWindDirectionString(int degree) {
	if (degree < 22.5) {
		return tr("N");
	} else if (degree < 67.5){
		return tr("NE");
	} else if (degree < 112.5) {
		return tr("E");
	} else if (degree < 157.5) {
		return tr("SE");
	} else if (degree < 202.5){
		return tr("S");
	} else if (degree < 247.5) {
		return tr("SW");
	} else if (degree < 292.5) {
		return tr("W");
	} else if (degree < 337.5){
		return tr("NW");
	} else {
		return tr("N");
	}
}

int cWeatherUtil::knToKmh (int kn) {
	return round(((double) kn) * 1.852);
}

int cWeatherUtil::calculateWindChill(int temperatureC, int windKn) {
	double windKmh = windKn * 1.852;
	return round(33.0 + (0.478 + 0.237 * sqrt(windKmh) - 0.0124 * windKmh) * (temperatureC  - 33.0));
}

int cWeatherUtil::round (double num) {
	return (int) floor(num + 0.5);
}

