/*
 * weather.c: A plugin for the Video Disk Recorder
 *
 * See the README file for copyright information and how to reach the author.
 *
 * $Id$
 */
#include "weather.h"
#include "display.h"
#include "simpleDisplay.h"
#include "i18n.h"
#include <ctype.h>
#include <climits>

static const char *ALLOWED_STATION_CHARS 	= "abcdefghijklmnopqrstuvwxyz";
static const char *ALLOWED_MAIL_CHARS 		= "abcdefghijklmnopqrstuvwxyz0123456789-.#~@";
static const char *VERSION = "0.2.1e";

cMenuSetupWeather::cMenuSetupWeather(cWeatherSetup *setup, cPluginWeather *plugin) {
	this->setup = setup;
	this->plugin = plugin;

	Add(new cMenuEditStrItem(tr("Station Id"), setup->stationId, 5, ALLOWED_STATION_CHARS));
	Add(new cMenuEditStrItem(tr("Username"), setup->user, 20, ALLOWED_MAIL_CHARS));
	Add(new cMenuEditStrItem(tr("Password (e-mail)"), setup->pwd, 20, ALLOWED_MAIL_CHARS));
	Add(new cMenuEditIntItem(tr("Updatetime (min)"), &setup->updateTime, 0, 10));
   Add(new cMenuEditBoolItem(tr("Use passive connection"), &setup->passiveFTP));
   Add(new cMenuEditBoolItem(tr("Display"), &setup->display, tr("Advanced"), tr("Simple")));
}

void cMenuSetupWeather::Store(void) {
	for(char *ptr = setup->stationId; *ptr; ++ptr) {
		if (islower(*ptr)) {
			*ptr = toupper(*ptr);
		}
	}

	SetupStore("StationId", setup->stationId);
	SetupStore("User", setup->user);
	SetupStore("Password", setup->pwd);
 	SetupStore("UpdateTime", setup->updateTime);
	SetupStore("PassiveFTP", setup->passiveFTP);
	SetupStore("Display", setup->display);
}


cPluginWeather::cPluginWeather(void) : metarReader(&setup){
}

cPluginWeather::~cPluginWeather() {
  // Clean up after yourself!
}

const char *cPluginWeather::Version(void) {
	return VERSION;
}

void cPluginWeather::handleConfigUpdate() {
	dsyslog("Config changed. Restarting metarReader.");
	metarReader.StartUpdate();
}

const char *cPluginWeather::CommandLineHelp(void) {
  // Return a string that describes all known command line options.
  return NULL;
}

bool cPluginWeather::ProcessArgs(int argc, char *argv[]) {
  // Implement command line argument processing here if applicable.
  return true;
}

bool cPluginWeather::Start(void) {
  RegisterI18n(Phrases);
  metarReader.StartUpdate();

  return true;
}

void cPluginWeather::Housekeeping(void)
{
  // Perform any cleanup or other regular tasks.
}

cOsdObject *cPluginWeather::MainMenuAction(void) {
	if (setup.display) {
  		return new cSimpleWeatherOsd(&metarReader);
	} else {
		return new cWeatherOsd(&metarReader);
	}
}

cMenuSetupPage *cPluginWeather::SetupMenu(void) {
  return new cMenuSetupWeather(&setup, this);
}

bool cPluginWeather::SetupParse(const char *Name, const char *Value) {
	if (!strcasecmp(Name, "StationId")) {
		strncpy(setup.stationId, Value, 4);
	} else if (!strcasecmp(Name, "User")) {
		strncpy(setup.user, Value, 99);
	} else if (!strcasecmp(Name, "Password")) {
		strncpy(setup.pwd, Value, 99);
	} else if (!strcasecmp(Name, "UpdateTime")) {
	   setup.updateTime = atoi(Value);
	} else if (!strcasecmp(Name, "Display")) {
	   setup.display = atoi(Value);
	} else if (!strcasecmp(Name, "PassiveFTP")) {
	   setup.passiveFTP = atoi(Value);
	} else {
		return false;
	}

	return true;
}


VDRPLUGINCREATOR(cPluginWeather); // Don't touch this!
