#ifndef SIMPLE_WEATHER_DISPLAY_H
#define SIMPLE_WEATHER_DISPLAY_H

#include "metarReader.h"
#include "weatherUtil.h"
#include "weather.h"

struct measurement {
	char description[500];
	char value[500];
};

class cSimpleWeatherOsd : public cOsdObject {
	public:
		cSimpleWeatherOsd(cUpdate *metarReader);
		~cSimpleWeatherOsd();
		virtual void Show(void);
		virtual eOSState ProcessKey(eKeys Key);
	protected:
		cOsdBase *osd;
		eDvbColor color;
		cUpdate *metarReader;
		virtual void printMetar (cOsdBase *osd, Decoded_METAR *Mptr, int x, int y);
		void drawData(cOsdBase *osd, Decoded_METAR *Mptr, int cy, cFont *font, int x, int y);
		int calculateMaxDescriptionFontWidth(measurement measurements[], int size, cFont *font);
		int calculateMaxValueFontWidth(measurement measurements[], int size, cFont *font);
		int calculateMaxFontWidth(char strings[][500], int size, cFont *font);
};

#endif //SIMPLE_WEATHER_DISPLAY_H
