#ifndef METAR_READER_H
#define METAR_READER_H

#include <unistd.h>
#include <vdr/thread.h>
#include <vdr/tools.h>
#include "setup.h"
#ifndef _FTPLIB_NO_COMPAT
#define _FTPLIB_NO_COMPAT
#endif

extern "C" {
	#include <metar.h>
	#include <ftplib.h>
}

class cUpdate : public cThread {
	private:
		bool active;
		bool fetched;
		char	metar[1024];
		char	date[1024];
		cWeatherSetup *setup;
		Decoded_METAR decodedMetar;
	public:
		cUpdate(cWeatherSetup *setup);
    	~cUpdate();
    	virtual void StartUpdate();
		const char *getMetar();
		Decoded_METAR *getDecodedMetar();
		const char *getDate();
		bool isDataAvailable();
		class NoDataAvailable {};
	protected:
   	virtual void Action(void);
		virtual void updateWeatherData();
		void logLastResponse(netbuf *conn);
};

#endif //METAR_READER_H

