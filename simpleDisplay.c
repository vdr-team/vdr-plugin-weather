#include "simpleDisplay.h"

const static char YES = '.';
const static int gapY = 2;
const static int gapX = 5;
const static int lineHeight = 2;



cSimpleWeatherOsd::cSimpleWeatherOsd(cUpdate *metarReader) {
  osd = NULL;
  color = clrRed;
  this->metarReader = metarReader;
}

cSimpleWeatherOsd::~cSimpleWeatherOsd() {
  delete osd;
}

void cSimpleWeatherOsd::Show(void) {
  osd = cOsd::OpenRaw(50, 50);
  if (osd) {
     osd->Create(0, 240, 612, 250, 4);
     osd->AddColor(clrBackground);
     osd->AddColor(clrRed);
     osd->AddColor(clrGreen);
     osd->AddColor(clrYellow);
     osd->AddColor(clrBlue);
     osd->Clear();

	if (metarReader->isDataAvailable()) {
		Decoded_METAR *decodedMetar = metarReader->getDecodedMetar();
		this->printMetar(osd, decodedMetar, 10, 250);
	} else {
		osd->Text(10, 250, tr("No Data available"));
	}
     osd->Flush();
  }
}

eOSState cSimpleWeatherOsd::ProcessKey(eKeys Key) {
  eOSState state = cOsdObject::ProcessKey(Key);

  if (state == osUnknown) {
     switch (Key & ~k_Repeat) {
       case kOk:     return osEnd;
		 case kBack:	return osEnd;
       default: return state;
     }

	  state = osContinue;
  }

  return state;
}

void cSimpleWeatherOsd::printMetar (cOsdBase *osd, Decoded_METAR *Mptr, int x, int y) {

	cFont *font = new cFont((eDvbFont)0);
	osd->SetFont((eDvbFont)0);
	drawData(osd, Mptr, 0, font, x, y);
}

int cSimpleWeatherOsd::calculateMaxDescriptionFontWidth(measurement measurements[], int size, cFont *font) {
	int maxSize = 0;
	int strSize = 0;

	for (int i = 0; i< size; i++) {
		strSize = font->Width(measurements[i].description);
		if (strSize > maxSize) {
			maxSize = strSize;
		}
	}

	return maxSize;
}

int cSimpleWeatherOsd::calculateMaxValueFontWidth(measurement measurements[], int size, cFont *font) {
	int maxSize = 0;
	int strSize = 0;

	for (int i = 0; i< size; i++) {
		strSize = font->Width(measurements[i].value);
		if (strSize > maxSize) {
			maxSize = strSize;
		}
	}

	return maxSize;
}

void cSimpleWeatherOsd::drawData(cOsdBase *osd, Decoded_METAR *Mptr, int cy, cFont *font, int x, int y) {
	measurement measurements[10];

	for(int i=0; i<10; i++) {
		*measurements[i].description = 0;
		*measurements[i].value = 0;
	}

   if ( Mptr->stnid[0] != '\0' ) {
      sprintf(measurements[cy].description, "%s", Mptr->stnid);
   }

   if ( Mptr->ob_hour != MAXINT && Mptr->ob_minute != MAXINT ) {
      sprintf(measurements[cy].description, "%s, %d:%d", measurements[cy].description, Mptr->ob_hour, Mptr->ob_minute);
  }

	cy++;
	sprintf(measurements[cy].description, "%s", tr("Temperature"));

   if ( Mptr->temp != MAXINT ) {
      sprintf(measurements[cy].value, "%d �C", Mptr->temp);
   } else {
		sprintf(measurements[cy].value, "%s", tr("No Data"));
	}

	cy++;
	sprintf(measurements[cy].description, "%s", tr("Windchill"));

   if (Mptr->temp != MAXINT && Mptr->winData.windSpeed != MAXINT) {
      sprintf(measurements[cy].value, "%d �C", cWeatherUtil::calculateWindChill(Mptr->temp, Mptr->winData.windSpeed));
   } else {
		sprintf(measurements[cy].value, "%s", tr("No Data"));
	}

	cy++;
	sprintf(measurements[cy].description, "%s", tr("Dew point"));

	if ( Mptr->dew_pt_temp != MAXINT ) {
      sprintf(measurements[cy].value, "%d �C", Mptr->dew_pt_temp);
   } else {
		sprintf(measurements[cy].value, "%s", tr("No Data"));
	}

	cy++;
	sprintf(measurements[cy].description, "%s", tr("Wind speed"));

   if ( Mptr->winData.windSpeed != MAXINT && Mptr->winData.windUnits[0] != '\0') {
      sprintf(measurements[cy].value, "%d %s",Mptr->winData.windSpeed, Mptr->winData.windUnits);
   } else {
		sprintf(measurements[cy].value, "%s", tr("No Data"));
	}

	cy++;
	sprintf(measurements[cy].description, "%s", tr("Wind direction"));

   if (Mptr->winData.windDir != MAXINT) {
      sprintf(measurements[cy].value, "%d� (%s)",Mptr->winData.windDir, cWeatherUtil::calculateWindDirectionString(Mptr->winData.windDir));
   } else {
		sprintf(measurements[cy].value, "%s", tr("No Data"));
	}

	cy++;
	sprintf(measurements[cy].description, "%s", tr("Humidity"));

	if ( Mptr->temp != MAXINT && Mptr->dew_pt_temp != MAXINT) {
		int hum = cWeatherUtil::calculateHumidity(Mptr->temp, Mptr->dew_pt_temp);

		sprintf(measurements[cy].value, "%i %%", hum);
	} else {
		sprintf(measurements[cy].value, "%s", tr("No Data"));
	}

	cy++;
	sprintf(measurements[cy].description, "%s", tr("Altimeter"));

   if ( Mptr->A_altstng ) {
      sprintf(measurements[cy].value, "%.2f inch", Mptr->inches_altstng );
   } else if ( Mptr->Q_altstng ) {
      sprintf(measurements[cy].value, "%d hPa", Mptr->hectoPasc_altstng );
   } else{
		sprintf(measurements[cy].value, "%s", tr("No Data"));
	}


	int maxSize = x + calculateMaxDescriptionFontWidth(measurements, cy, font) + gapX;
	osd->Text(maxSize-font->Width(measurements[0].description)-gapX, y, measurements[0].description);
	y += font->Height(measurements[0].description) + gapY;
	int lineY = y;
	osd->Fill(x, y, maxSize+gapX, y+lineHeight, clrBlue);
	y += lineHeight+gapY;

	for (int i=1; i<=cy; i++) {
		osd->Text(maxSize-font->Width(measurements[i].description)-gapX, y, measurements[i].description);
		osd->Text(maxSize+lineHeight+gapX+gapX, y, measurements[i].value);
		osd->Fill(maxSize, y+(font->Height(measurements[0].description)/2), maxSize+gapX, y+(font->Height(measurements[0].description)/2)+lineHeight, clrBlue);
		y += font->Height(measurements[0].description) + gapY;
	}

	osd->Fill(maxSize+gapX, lineY, maxSize+gapX+lineHeight, y, clrBlue);
}
