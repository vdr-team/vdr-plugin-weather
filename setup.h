#ifndef WEATHER_SETUP_H
#define WEATHER_SETUP_H

#include <string.h>

class cWeatherSetup {
	public:
		char stationId[5];
		char user[100];
		char pwd[100];
		int  updateTime;
		int display;
		int passiveFTP;
		cWeatherSetup(void);
};

#endif //WEATHER_SETUP_H
