/*
 * i18n.c: Internationalization
 *
 * See the README file for copyright information and how to reach the author.
 *
 * $Id: i18n.c 1.3 2002/06/23 13:05:59 kls Exp $
 */

#include "i18n.h"

const tI18nPhrase Phrases[] = {
  { "Weather",
    "Wetter",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "M�t�o",
    "",// TODO
    "S��",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
  },
  { "Station Id",
    "Stations Id",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "Station Id",
    "",// TODO
    "Asematunnus",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
  },
  { "Username",
    "Anmeldename",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "Utilisateur",
    "",// TODO
    "K�ytt�j�tunnus",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
  },
  { "Password (e-mail)",
    "Passwort (E-Mail)",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "Mot de passe (e-mail)",
    "",// TODO
    "Salasana (e-mail)",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
  },
  { "Updatetime (min)",
    "Aktualisierungsintervall (min)",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "Mise � jour (min)",
    "",// TODO
    "P�ivitysv�li (min)",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
  },
  { "Wind",
    "Wind",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "Vent",
    "",// TODO
    "Tuuli",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
  },
  { "Wind speed",
    "Windgeschwindigkeit",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "Vitesse du vent",
    "",// TODO
    "Tuulen nopeus",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
  },
  { "Wind direction",
    "Windrichtung",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "Direction du vent",
    "",// TODO
    "Tuulen suunta",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
  },
  { "Temperature",
    "Temperatur",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "Temp�rature",
    "",// TODO
    "L�mp�tila",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
  },
  { "Windchill",
    "Gef�hlte Temperatur",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "Temp�rature du vent",
    "",// TODO
    "Tuulivaikutus",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
  },
  { "Dew point",
    "Taupunkt",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "Point de condensation",
    "",// TODO
    "Kastepiste",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
  },
  { "Humidity",
    "Luftfeuchte",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "Humidit�",
    "",// TODO
    "Ilmankosteus",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
  },
  { "Altimeter",
    "Luftdruck",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "Altim�tre ",
    "",// TODO
    "Ilmanpaine",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
  },
  { "N",
    "N",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "Nord",
    "",// TODO
    "Pohjoinen",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
  },
  { "NE",
    "NO",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "N-O",
    "",// TODO
    "Koillinen",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
  },
  { "E",
    "O",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "Est",
    "",// TODO
    "It�",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
  },
  { "SE",
    "SO",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "S-E",
    "",// TODO
    "Kaakko",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
  },
  { "S",
    "S",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "Sud",
    "",// TODO
    "Etel�",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
  },
  { "SW",
    "SW",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "S-O",
    "",// TODO
    "Lounas",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
  },
  { "W",
    "W",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "Ouest",
    "",// TODO
    "L�nsi",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
  },
  { "NW",
    "NW",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "N-W",
    "",// TODO
    "Luode",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
  },
  { "Display",
    "Anzeige",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "Affichage",
    "",// TODO
    "N�ytt�",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
  },
  { "Simple",
    "Einfach",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "Simple",
    "",// TODO
    "Yksinkertainen",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
  },
  { "Advanced",
    "Erweitert",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "Avanc�",
    "",// TODO
    "Laajennettu",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
  },
  { "Displays the current weather conditons.",
    "Zeigt das aktuelle Wetter",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "Affiche la m�t�o courante",
    "",// TODO
    "N�ytt�� t�m�nhetkisen s��tilan",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
  },
  { "On",
    "An",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "En marche",
    "",// TODO
    "P��ll�",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
  },
  { "Off",
    "Aus",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "Arr�t",
    "",// TODO
    "Poissa",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
  },
  { "Use passive connection",
    "Benutzte passive Verbindung",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "Utiliser une connection passive",
    "",// TODO
    "K�yt� passiivista yhteytt�",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
  },
  { "No Data available",
    "Keine Daten verf�gbar",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "Aucune donn�e utilisable",
    "",// TODO
    "Ei tietoja saatavilla",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
  },
  { NULL }
  };
