
#include "display.h"
#include "windrose.xpm"
#include "e.xpm"
#include "se.xpm"
#include "sun.xpm"
#include "border.xpm"

const static char YES = '.';
//const static char *POSSIBLE_CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ�";
const static int gapY = 2;
const static int gapX = 5;
const static int lineHeight = 2;

cWeatherOsd::cWeatherOsd(cUpdate *metarReader) : cSimpleWeatherOsd(metarReader) {
}


void cWeatherOsd::initBitmap(cBitmap &b, char*xpm[], eDvbColor col, bool setBg) {
   for (int y=0; y<100; y++) {
      for (int x=0; x<100; x++) {
			if (setBg) {
         	b.SetPixel(x, y, (xpm[y][x] == YES) ? col : clrBackground);
			} else {
				if (xpm[y][x] == YES) {
					b.SetPixel(x, y, col);
				}
			}
      }
   }
}

void cWeatherOsd::initEastBitmap(cBitmap &b, eDvbColor col) {
   for (int y=0; y<100; y++) {
      for (int x=0; x<100; x++) {
			if (e_xpm[y][x] == YES) {
				b.SetPixel(x, y, col);
			}
      }
   }
}

void cWeatherOsd::initNorthBitmap(cBitmap &b, eDvbColor col) {
for (int y=0; y<100; y++) {
      for (int x=0; x<100; x++) {
			if (e_xpm[y][x] == YES) {
				b.SetPixel(y, 100 - x, col);
			}
      }
   }
}

void cWeatherOsd::initWestBitmap(cBitmap &b, eDvbColor col) {
   for (int y=0; y<100; y++) {
      for (int x=99; x>=0; x--) {
			if (e_xpm[y][x] == YES) {
				b.SetPixel(100 - x, y, col);
			}
      }
   }
}

void cWeatherOsd::initSouthBitmap(cBitmap &b, eDvbColor col) {
	for (int y=0; y<100; y++) {
      for (int x=0; x<100; x++) {
			if (e_xpm[y][x] == YES) {
				b.SetPixel(y, x, col);
			}
      }
   }
}

void cWeatherOsd::initSouthEastBitmap(cBitmap &b, eDvbColor col) {
	for (int y=0; y<100; y++) {
      for (int x=0; x<100; x++) {
			if (se_xpm[y][x] == YES) {
				b.SetPixel(x, y, col);
			}
      }
   }
}

void cWeatherOsd::initNorthEastBitmap(cBitmap &b, eDvbColor col) {
   for (int y=0; y<100; y++) {
      for (int x=0; x<100; x++) {
			if (se_xpm[y][x] == YES) {
				b.SetPixel(y, 100 - x, col);
			}
      }
   }
}


void cWeatherOsd::initNorthWestBitmap(cBitmap &b, eDvbColor col) {
	for (int y=0; y<100; y++) {
      for (int x=0; x<100; x++) {
			if (se_xpm[y][x] == YES) {
				b.SetPixel(100-x, 100-y, col);
			}
      }
   }
}

void cWeatherOsd::initSouthWestBitmap(cBitmap &b, eDvbColor col) {
	for (int y=0; y<100; y++) {
      for (int x=99; x>=0; x--) {
			if (se_xpm[y][x] == YES) {
				b.SetPixel(100 - x, y, col);
			}
      }
   }
}

void cWeatherOsd::initBorderBitmap(cBitmap &b, eDvbColor col) {
	for (int y=0; y<100; y++) {
      for (int x=99; x>=0; x--) {
			if (border_xpm[x][y] == YES) {
				b.SetPixel(x, y, col);
			}
      }
   }
}

void cWeatherOsd::printMetar (cOsdBase *osd, Decoded_METAR *Mptr, int x, int y) {
	cFont *font = new cFont((eDvbFont)0);
	osd->SetFont((eDvbFont)0);
	//int cy = 0;


	int lineY = y;

	osd->Text(x + 80 - font->Width(tr("Weather")) - gapX, y, tr("Weather"));
	lineY += font->Height(tr("Weather")) + gapY;

	osd->Fill(x, lineY, x+80, lineY+lineHeight, clrBlue);
	osd->Fill(x+80, lineY, x+80+lineHeight, lineY+20, clrBlue);
	drawWeather(osd, Mptr, x, lineY+20);
	x += 120;

	osd->Text(x + 80 - font->Width(tr("Wind")) - gapX, y, tr("Wind"));
	osd->Fill(x, lineY, x+80, lineY+lineHeight, clrBlue);
	osd->Fill(x+80, lineY, x+80+lineHeight, lineY+20, clrBlue);

	if (Mptr->winData.windDir != MAXINT) {
		drawWindRose(osd, Mptr->winData.windDir, x, lineY+20);
	}

	x += 120;

	drawData(osd, Mptr, 0, font, x, y);
}

void cWeatherOsd::drawWeather(cOsdBase *osd, Decoded_METAR *mptr, int x, int y) {
	//char* filename;

//	sprintf("%s%s.xpm", coverage[mptr->], phenomen[]);
	cBitmap weather(100, 100, 3, false);
	initBitmap(weather, sun_xpm, clrYellow, true);
	initBorderBitmap(weather, clrBlue);
	osd->SetBitmap(x, y, weather);
}

void cWeatherOsd::drawWindRose(cOsdBase *osd, int degree, int x, int y) {
	cBitmap windRose(100, 100, 3, false);
	initBitmap(windRose, windrose_xpm, clrYellow, true);

	if (degree < 22.5) {
		initNorthBitmap(windRose, clrBlue);
	} else if (degree < 67.5){
		initNorthEastBitmap(windRose, clrBlue);;
	} else if (degree < 112.5) {
		initEastBitmap(windRose, clrBlue);;
	} else if (degree < 157.5) {
		initSouthEastBitmap(windRose, clrBlue);;
	} else if (degree < 202.5){
		initSouthBitmap(windRose, clrBlue);;
	} else if (degree < 247.5) {
		initSouthWestBitmap(windRose, clrBlue);;
	} else if (degree < 292.5) {
		initWestBitmap(windRose, clrBlue);;
	} else if (degree < 337.5){
		initNorthWestBitmap(windRose, clrBlue);;
	} else {
		initNorthBitmap(windRose, clrBlue);;
	}

	initBorderBitmap(windRose, clrBlue);
	osd->SetBitmap(x, y, windRose);
}
