#ifndef WEATHER_UTIL_H
#define WEATHER_UTIL_H

class cWeatherUtil {
public:
	static double calculateSaturationVaporPressure(double temperature);
	static int calculateHumidity(double temperature, double dewPoint);
	static const char *calculateWindDirectionString(int degree);
	static int knToKmh(int kn);
	static int calculateWindChill(int temperatureC, int windKn);
protected:
	static int round(double num);
	};

#endif //WEATHER_UTIL_H
